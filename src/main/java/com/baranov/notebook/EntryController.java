package com.baranov.notebook;

import com.baranov.notebook.domain.Entry;
import com.baranov.notebook.repos.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class EntryController {

    @Autowired
    private Repository repository;

    @GetMapping
    public String main(Map<String, Object> model) {
        Iterable<Entry> entries = repository.findAll();
        model.put("entries", entries);
        return "main";
    }

    @GetMapping("search")
    public String search(@RequestParam String search, Map<String, Object> model) {
        Iterable<Entry> entries;

        if (search != null && !search.isEmpty()) {
            entries = repository.findByPhone(search);
        } else {
            entries = repository.findAll();
        }

        model.put("entries", entries);

        return "main";
    }

    @GetMapping("filter")
    public String filter(@RequestParam String position, Map<String, Object> model) {
        Iterable<Entry> entries;

        if (position.equals("allPosition")) {
            entries = repository.findAll();
        } else {
            entries = repository.findByPosition(position);
        }

        model.put("entries", entries);

        return "main";
    }

    @PostMapping
    public String add(@RequestParam String fio, @RequestParam String phone, @RequestParam String email,
                      @RequestParam String address, @RequestParam String company, @RequestParam String position,
                      Map<String, Object> model) {
        Entry entry = new Entry(fio, phone, email, address, company, position);

        repository.save(entry);

        Iterable<Entry> entries = repository.findAll();
        model.put("entries", entries);

        return "main";
    }

    @PostMapping("delete")
    public String delete(@RequestParam Integer id, Map<String, Object> model) {
        repository.deleteById(id);

        Iterable<Entry> entries = repository.findAll();
        model.put("entries", entries);

        return "main";
    }

    @PostMapping("edit")
    public String edit(@RequestParam Integer id, @RequestParam String fio, @RequestParam String phone, @RequestParam String email,
                       @RequestParam String address, @RequestParam String company, @RequestParam String position,
                       Map<String, Object> model) {

        repository.save(new Entry(id, fio, phone, email, address, company, position));

        Iterable<Entry> entries = repository.findAll();
        model.put("entries", entries);

        return "main";
    }
}
