package com.baranov.notebook.repos;

import com.baranov.notebook.domain.Entry;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface Repository extends CrudRepository<Entry, Integer> {

    List<Entry> findByPhone(String phone);

    List<Entry> findByPosition(String position);
}

