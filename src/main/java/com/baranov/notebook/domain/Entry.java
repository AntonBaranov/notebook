package com.baranov.notebook.domain;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Entry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column()
    private Integer id;

    @Column(unique = true)
    private String fio;

    @Column(length = 30)
    private String phone;

    @Column()
    private String email;

    @Column()
    private String address;

    @Column(length = 100)
    private String company;

    @Column()
    private String position;

    @ColumnDefault("CURRENT_TIMESTAMP(6)")
    @Column(nullable = false, updatable = false)
    private LocalDateTime created = LocalDateTime.now();

    @ColumnDefault("CURRENT_TIMESTAMP(6)")
    @Column(nullable = false)
    private LocalDateTime updated = LocalDateTime.now();

    public Entry() {
    }

    public Entry(String fio, String phone, String email, String address, String company, String position) {
        this.fio = fio;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.company = company;
        this.position = position;
    }

    public Entry(Integer id, String fio, String phone, String email, String address, String company, String position) {
        this.id = id;
        this.fio = fio;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.company = company;
        this.position = position;
    }

    public Integer getId() {
        return id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }
}
